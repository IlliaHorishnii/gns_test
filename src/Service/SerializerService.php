<?php

namespace App\Service;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class SerializerService
{
    /**
     * @var Serializer
     */
    protected Serializer $serializer;

    /**
     * Add additional normalizers/encoders
     */
    public function __construct()
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader());
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];
        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer($classMetadataFactory, new CamelCaseToSnakeCaseNameConverter(), null, null, null, null, $defaultContext)
        ];
        $encoders = [
            new JsonEncoder(),
            new XmlEncoder()
        ];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * @param mixed $object
     * @param array $context
     * @return string
     */
    public function toJson(mixed $object, array $context = []): string
    {
        return $this->serializer->serialize($object, 'json', $context);
    }

    /**
     * @param mixed $object
     * @param array $context
     * @return array
     * @throws ExceptionInterface
     */
    public function toArray(mixed $object, array $context = []): array
    {
        return $this->serializer->normalize($object, null, $context);
    }

    /**
     * @param array $data
     * @param string $className
     * @param mixed $object
     * @param array $context
     * @return mixed
     * @throws ExceptionInterface
     */
    public function fromArray(array $data, string $className, mixed $object, array $context = []): mixed
    {
        if (is_object($object) && get_class($object) === $className) {
            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $object;
        }
        return $this->serializer->denormalize($data, $className, null, $context);
    }
}
