<?php

namespace App\Helper;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator
{
    /** @var ValidatorInterface  */
    private static $validator = null;

    public static function validate(array $value , Collection $constraints)
    {
        $violations = self::getValidator()->validate($value, $constraints);
        if (count($violations) > 0) {
            $prepared = [];
            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                /** @todo add translations */
                $prepared[$violation->getPropertyPath()] = sprintf(
                    "%s: %s",
                    $violation->getPropertyPath(),
                    $violation->getMessage()
                );
            }


            throw new ValidatorException(json_encode($prepared));
        }
    }

    /**
     * @return ValidatorInterface
     */
    private static function getValidator()
    {
        if (empty(self::$validator) || !(self::$validator instanceof Validation)) {
            self::$validator = Validation::createValidator();
        }

        return self::$validator;
    }
}
