<?php

namespace App\Controller\Api;

use App\Entity\Product;
use App\Helper\Validator;
use App\Repository\ProductRepository;
use App\Service\SerializerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Exception\ValidatorException;

#[Route('/api/product')]
class ProductController extends AbstractController
{

    private Request $request;

    public function __construct(
        private ProductRepository $productRepository,
        private SerializerService $serializer,
        RequestStack $requestStack
    )
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    #[Route('', name: 'product_list', methods: ['GET'])]
    public function list(): JsonResponse
    {
        try {
            $products = $this->productRepository->findAll();
            $productsJson = $this->serializer->toJson($products);
        } catch (\Throwable $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

        return new JsonResponse($productsJson, 200);
    }

    #[Route('/{id}', name: 'product_retrieve', methods: ['GET'])]
    public function retrieve(): JsonResponse
    {
        if(!$product = $this->productRepository->findOneBy(['id' => $this->request->get("id")])) {
            return new JsonResponse('Product not found', 404);
        }

        try {
            $productsJson = $this->serializer->toJson($product);
        } catch (\Throwable $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

        return new JsonResponse($productsJson, 200);
    }

    #[Route('/barcode/{barcode}', name: 'product_retrieve_by_barcode', methods: ['GET'])]
    public function barcodeRetrieve(): JsonResponse
    {
        if(!$product = $this->productRepository->findOneBy(['barcode' => $this->request->get("barcode")])) {
            return new JsonResponse('Product not found', 404);
        }

        try {
            $productsJson = $this->serializer->toJson($product);
        } catch (\Throwable $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

        return new JsonResponse($productsJson, 200);
    }

    #[Route('/{id}', name: 'product_delete', methods: ['DELETE'])]
    public function delete(): JsonResponse
    {
        if(!$product = $this->productRepository->findOneBy(['id' => $this->request->get("id")])) {
            return new JsonResponse('Product not found', 404);
        }

        try {
            $this->productRepository->remove($product, true);
        } catch (\Throwable $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

        return new JsonResponse('success', 200);
    }

    #[Route('/{id}', name: 'product_update', methods: ["PUT"])]
    public function update(): JsonResponse
    {
        if(!$product = $this->productRepository->findOneBy(['id' => $this->request->get("id")])) {
            return new JsonResponse('Product not found', 404);
        }

        try {
            $productData = $this->request->toArray();
            $constraints = new Assert\Collection([
                'name' => [new Assert\Optional(new Assert\Type(['type' => ['type' => 'string']]))],
                'barcode' => [new Assert\Optional(new Assert\Type(['type' => ['type' => 'string']]))],
                'price' => [new Assert\Optional()],
            ]);

            Validator::validate($productData, $constraints);
        } catch (ValidatorException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 406);
        }

        try {
            $this->fillEntity($productData, $product);
            $this->productRepository->add($product, true);
            $productsJson = $this->serializer->toJson($product);
        } catch (\Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse($productsJson, 200);
    }

    #[Route('', name: 'product_create', methods: ['POST'])]
    public function create(): JsonResponse
    {
        try {
            $productData = $this->request->toArray();
            $constraints = new Assert\Collection([
                'name' => [new Assert\Required(new Assert\Type(['type' => ['type' => 'string']]))],
                'barcode' => [new Assert\Required(new Assert\Type(['type' => ['type' => 'string']]))],
                'price' => [new Assert\Optional()],
            ]);

            Validator::validate($productData, $constraints);
        } catch (ValidatorException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 406);
        }

        try {
            $product = new Product();
            $this->fillEntity($productData, $product);
            $this->productRepository->add($product, true);
            $productsJson = $this->serializer->toJson($product);
        } catch (\Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse($productsJson, 200);
    }

    private function fillEntity(array $productData, Product $product)
    {
        if(!empty($productData['name'])) $product->setName($productData['name']);
        if(!empty($productData['barcode'])) $product->setBarcode($productData['barcode']);
        if(array_key_exists('price', $productData)) $product->setPrice($productData['price']);
    }
}
