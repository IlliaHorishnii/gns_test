# GNS_Test



## API

GET /api/product - get list of products.

GET /api/product/{id} - retrieve product.

GET /api/product/barcode/{barcode} - retrieve product by barcode.

DELETE /api/product/{id} - delete product.

POST /api/product - create product.

PUT /api/product/{id} - update product.

